# Fiche

Programme présent sur les machines de l'ENS, permettant un accès en
ligne de commande à la fiche annuaire de quelqu'un.

## Options

Options génériques :

* `-h` : affiche de l'aide et quitte.
* `-f FORMAT`, `--format FORMAT` : formate selon la chaîne de formatage donnée,
	voir plus bas ou la page de man.
* `--json` : ne formate rien, se contente d'afficher du json brut.

### Mode par défaut (fetch)

`fiche [fetch] [-h] [--json] [-n] [--nick] [-f FORMAT] login`

* `-n`, `--nom` : affiche le nom complet seulement.
* `--nick` : affiche les surnoms seulement.
* `login` : le login clipper recherché.

### Mode recherche

`fiche search [-h] [--json] [-f FORMAT] [-n nom] [-p prenom] [-y annee]
 [-P promo] ...`

* `-n` : recherche un nom de famille
* `-p` : recherche un prénom
* `-y` : recherche par année d'entrée
* `-P` : recherche par promo (eg. `info`)
* `...` : recherche dans un peu tous les champs

## Chaîne de formatage

Il est possible, en passant une option `-f FORMAT` ou `--format FORMAT` à
`fiche`, de choisir le format de sortie utilisé. La chaîne donnée est une
chaîne quelconque, pouvant contenir les littéraux suivants :

* `%%` : un `%` littéral ;
* `%p` : le prénom ;
* `%f` : le nom de famille ;
* `%n` : le nom complet ;
* `%y` : l'année d'entrée à l'ENS ;
* `%#s` : le nombre de surnoms de la personne ;
* `%[0-9]+s` : le i-ème surnom de la personne, ou rien si elle n'en a pas tant ;
* `%'...'s`, où `...` est une chaîne quelconque (sans formatteurs) : la liste
	des surnoms de la personne, séparés par `...` ;
* `%l` : le login clipper ;
* `%P` : la promotion.

## Plus de documentation ?

Lisez la page de man (`man/fiche.1`).
