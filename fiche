#!/usr/bin/env python3

import argparse
import json
import sys
import urllib.request

HOSTNAME = 'https://www.eleves.ens.fr/annuaire/'
URL_FETCH_PATTERN = HOSTNAME + 'fiches/json/{}'
URL_FETCH_MULTI = HOSTNAME + 'fiches/json_multi'
URL_SEARCH = HOSTNAME + 'fiches/json_search/'
HUMAN_READABLE_FORMATTER = \
    "Nom : %n\nAnnée : %y\nSurnoms : %', 's\nClipper : %l\nPromo : %P\n"
DEFAULT_SEARCH_RESULT_FORMATTER = \
    "[%l] %n (%P)"
NOARG_FORMAT_KEYS = {
    'p': 'firstname',
    'f': 'lastname',
    'y': 'year',
    'l': 'login',
    'P': 'promotion',
    }


class BadFormatter(Exception):
    pass


def getOptions():
    """ Reads command-line options. """

    parser = argparse.ArgumentParser(
        description='Récupère des informations sur une personne dans sa fiche '
        + 'annuaire.')
    global_parser = argparse.ArgumentParser(add_help=False)
    global_parser.add_argument('-f', '--format',
                        help="utilise la chaîne de formatage donnée comme "
                        + "format de sortie (cf `man fiche`).")
    global_parser.add_argument('--json', action='store_true',
                        help="ne formate rien, se contente d'afficher du json "
                        + "brut.")
    sParser = parser.add_subparsers(dest='subparser_name')
    search_parser = sParser.add_parser('search', parents=[global_parser],
                                       help="Recherche dans l'annuaire.")
    fetch_parser = sParser.add_parser('fetch', parents=[global_parser],
                                      help="Lit une fiche dans l'annuaire.")

    fetch_parser.add_argument('-n', '--nom', action='store_true',
                        help="affiche le nom complet seulement.")
    fetch_parser.add_argument('--nick', action='store_true',
                        help="affiche les surnoms seulement.")
    fetch_parser.add_argument('login',
                        help="recherche le login exact donné sur l'annuaire.")
    fetch_parser.set_defaults(func=fetchFiche)
    search_parser.add_argument('-n', metavar='nom',
                        help="recherche le nom de famille donné")
    search_parser.add_argument('-p', metavar='prenom',
                        help="recherche le prénom donné")
    search_parser.add_argument('-y', metavar='annee',
                        help="recherche dans l'année d'entrée donnée")
    search_parser.add_argument('-P', metavar='promo',
                        help="recherche dans la promotion donnée")
    search_parser.add_argument('ref', nargs=argparse.REMAINDER,
                        help="recherche la référence donnée un peu partout.")
    search_parser.set_defaults(func=doSearch)

    if 'fetch' not in sys.argv[1:] and 'search' not in sys.argv[1:] \
            and not '--help' in sys.argv[1:] and not '-h' in sys.argv[1:]:
        out = parser.parse_args(['fetch']+sys.argv[1:])
    else:
        out = parser.parse_args()

    if out.subparser_name == 'fetch':
        if not out.login.isalnum():
            print('ERREUR: un login doit être alphanumérique.',
                  file=sys.stderr)
            sys.exit(1)

    return out


def getPage(url, postVals=None, isJson=False):
    """ Returns the contents of a page, or fails and exits on error. """
    try:
        if postVals is not None:
            if isJson:
                req = urllib.request\
                    .Request(url, json.dumps(postVals).encode('ascii'))
                req.add_header('Content-Type', 'application/json')
            else:
                postData = urllib.parse.urlencode(postVals).encode('ascii')
                req = urllib.request.Request(url, postData)
        else:
            req = urllib.request.Request(url)
        handle = urllib.request.urlopen(req)
        content = handle.read()
        handle.close()
        return content.decode('unicode-escape')
    except urllib.error.HTTPError as e:
        # TODO handle more errors? (eg. login required)
        if e.code == 404:  # Not found: login not in the database.
            print("Login inconnu.", file=sys.stderr)
            sys.exit(2)  # User fail
        print("Could not retrieve page: {}".format(str(e)), file=sys.stderr)
        sys.exit(1)  # Server fail?


def makeNbsp(s):
    """ Replaces all spaces in `s` by a non-breakable space. """
    return s.replace(' ', '\u00A0')


def formatedFiche(formatter, vals):
    """ Displays the given values, formatted by `formatter`. """
    pos = 0
    outStr = ""
    while pos < len(formatter):
        ch = formatter[pos]
        if ch != '%':
            outStr += ch
        else:
            if pos + 1 >= len(formatter):
                print("Error: invalid formatter string, orphaned '%'.",
                      file=sys.stderr)
                raise BadFormatter
            pos += 1
            nCh = formatter[pos]

            if nCh in NOARG_FORMAT_KEYS:
                outStr += str(vals[NOARG_FORMAT_KEYS[nCh]])
            elif nCh == 'n':
                outStr += '{} {}'.format(vals['firstname'], vals['lastname'])
            else:
                # NOTE so far we only have '%s' handling arguments.
                # We WILL have to change this 'else' if we handle more of them
                sPos = formatter[pos:].find('s')
                if sPos < 0:
                    raise BadFormatter
                args = formatter[pos:pos+sPos]
                pos += sPos

                if args == '#':
                    outStr += str(len(vals['surnoms']))
                elif args[0] == args[-1] == "'" and len(args) > 1:
                    if len(vals['surnoms']) > 0:
                        outStr += vals['surnoms'][0]
                    for nick in vals['surnoms'][1:]:
                        outStr += '{}{}'.format(args[1:-1], nick)
                elif args.isnumeric():
                    nickId = int(args)
                    if nickId < len(vals['surnoms']):
                        outStr += vals['surnoms'][nickId]
                else:
                    raise BadFormatter
        pos += 1

    print(outStr, end='')
    if len(outStr) > 0 and outStr[-1] != '\n':
        print('')


def humanReadableFiche(vals):
    """ Displays a human-readable 'fiche'. Default behaviour. """
    formatedFiche(HUMAN_READABLE_FORMATTER, vals)


def fetchLoginJson(login):
    url = URL_FETCH_PATTERN \
        .format(login)  # We've already checked that `login` is valid.
    jsonVal = json.loads(getPage(url))

    return jsonVal


def fetchFiche(parsed):
    jsonVal = fetchLoginJson(parsed.login)

    if parsed.json:  # Only display raw json
        print(json.dumps(jsonVal))  # Convert back and forth: it is valid json.
    elif parsed.nom:  # Only display name
        print('{} {}'.format(
            makeNbsp(jsonVal['firstname']),
            makeNbsp(jsonVal['lastname'])))
    elif parsed.nick:  # Only display nicknames
        for nick in jsonVal['surnoms']:
            print(nick)
    elif parsed.format:  # Format string
        try:
            formatedFiche(parsed.format, jsonVal)
        except BadFormatter:
            print('Chaîne de formatage invalide.', file=sys.stderr)
            sys.exit(2)
    else:  # No particular option: human-readable 'fiche'
        humanReadableFiche(jsonVal)


def doSearch(parsed):
    searchDict = {}
    searchDict['firstname'] = parsed.p
    searchDict['lastname'] = parsed.n
    searchDict['promotion'] = parsed.P
    searchDict['year'] = parsed.y
    totRef = ''
    for ref in parsed.ref:
        totRef += ref + ' '
    if len(totRef) > 0:
        totRef = totRef[:-1]
    searchDict['ref'] = totRef
    searchDict = {k: v for k, v in searchDict.items() if v}

    jsonResp = json.loads(getPage(URL_SEARCH, searchDict))

    if parsed.json:
        print(json.dumps(jsonResp))
        return

    logins = jsonResp['logins']

    fiches = json.loads(getPage(URL_FETCH_MULTI,
                                postVals={'logins': logins}, isJson=True))
    if parsed.format:
        fmt = parsed.format
    else:
        fmt = DEFAULT_SEARCH_RESULT_FORMATTER

    for login in logins:
        if not fiches[login]:  # 404?!
            print(login)
        try:
            formatedFiche(fmt, fiches[login])
        except BadFormatter:
            print('Chaîne de formatage invalide.', file=sys.stderr)
            sys.exit(2)


def main():
    parsed = getOptions()
    parsed.func(parsed)


if __name__ == '__main__':
    main()
